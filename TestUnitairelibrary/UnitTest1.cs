using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestUnitaire;

namespace TestUnitairelibrary
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Assert.AreEqual(MaSuperClasse.Multiplier(42), 84);
            Assert.AreNotEqual(MaSuperClasse.Multiplier(42), 42);
        }
    }
}
